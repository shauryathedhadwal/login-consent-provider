import { AdminApi, Configuration } from '@oryd/hydra-client'

const baseOptions: any = {}
const BASE_URL = process.env.HYDRA_ADMIN_URL

if (process.env.MOCK_TLS_TERMINATION) {
  baseOptions.headers = { 'X-Forwarded-Proto': 'https' }
}

const hydraAdmin = new AdminApi(
  new Configuration({
    basePath: BASE_URL,
    baseOptions,
  })
)

export { hydraAdmin }
