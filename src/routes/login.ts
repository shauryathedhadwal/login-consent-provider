import express, { NextFunction, Request, Response } from 'express'
import url from 'url'
import urljoin from 'url-join'
import csrf from 'csurf'
import { validateCredentials } from '../rpc/userManager'
import { hydraAdmin } from '../config'

const csrfProtection = csrf({ cookie: true })
const router = express.Router()

router.get('/', csrfProtection, (req, res, next) => {
  const query = url.parse(req.url, true).query
  // The challenge is used to fetch information about the login request from ORY Hydra.
  const challenge = String(query.login_challenge)
  if (!challenge) {
    return next(new Error('Expected a login challenge to be set but received none.'))
  }

  hydraAdmin
    .getLoginRequest(challenge)
    .then(({ data: body }) => {
      if (body.skip) {
        // TODO: Store session in db with user details like login time, source, ip etc
        return hydraAdmin
          .acceptLoginRequest(challenge, {
            subject: String(body.subject)
          })
          .then(({ data: body }) => {
            res.redirect(String(body.redirect_to)) // Redirect to hydra
          })
      }

      let mobile = body?.oidc_context?.login_hint ? parseInt(body.oidc_context.login_hint) : undefined

      // If authentication can't be skipped we MUST show the login UI.
      res.render('login', {
        csrfToken: req.csrfToken(),
        challenge: challenge,
        action: urljoin(process.env.BASE_URL || '', '/login'),
        hint: body.oidc_context?.login_hint || '',
        mobile: mobile
      })
    })
    // This will handle any error that happens when making HTTP calls to hydra
    .catch(next)
})

router.post('/', csrfProtection, async (req, res, next) => {
  // The challenge is now a hidden input field, so let's take it from the request body instead
  const challenge = req.body.challenge

  if (req.body.submit === 'Deny access') {
    // Looks like the consent request was denied by the user
    return (
      hydraAdmin
        .rejectLoginRequest(challenge, {
          error: 'access_denied',
          error_description: 'The resource owner denied the request'
        })
        .then(({ data: body }) => {
          // All we need to do now is to redirect the browser back to hydra!
          res.redirect(String(body.redirect_to))
        })
        // This will handle any error that happens when making HTTP calls to hydra
        .catch(next)
    )
  }

  const { phonenumber, password } = req.body
  let response = { data: { success: false } }

  try {
    // TODO: Plugin any validation scheme
    response = await validateCredentials(phonenumber, password)
  } catch (e) {
    console.error(e)
  }

  if (!response?.data?.success) {
    return res.render('login', {
      csrfToken: req.csrfToken(),
      challenge: challenge,
      error: 'Mobile number or password is incorrect'
    })
  }

  hydraAdmin
    .getLoginRequest(challenge)
    .then(({ data: loginRequest }) =>
      hydraAdmin
        .acceptLoginRequest(challenge, {
          subject: phonenumber,
          remember: true,
          // When the session expires, in seconds. Set this to 0 so it will never expire.
          remember_for: +(process.env.LOGIN_SESSION_EXPIRES_IN_SECONDS || '0'),
          // Sets which "level" (e.g. 2-factor authentication) of authentication the user has. The value is really arbitrary
          // and optional. In the context of OpenID Connect, a value of 0 indicates the lowest authorization level.
          acr: '0'
          // If that variable is not set, the ACR value will be set to the default passed here ('0')
        })
        .then(({ data: body }) => {
          res.redirect(String(body.redirect_to)) // Redirect to Hydra
        })
    )
    // This will handle any error that happens when making HTTP calls to hydra
    .catch(next)
})

export default router
