import express from 'express'
import url from 'url'
import csrf from 'csurf'
import { hydraAdmin } from '../config'
import urljoin from 'url-join'

const csrfProtection = csrf({ cookie: true })
const router = express.Router()

router.get('/', csrfProtection, (req, res, next) => {
  const query = url.parse(req.url, true).query

  const challenge = String(query.logout_challenge)
  if (!challenge) {
    next(new Error('Expected a logout challenge to be set but received none.'))
    return
  }

  hydraAdmin
    .getLogoutRequest(challenge)
    .then(({ data: body }) => {

      if (process.env.SKIP_LOGOUT_CONSENT === '1') {
        return hydraAdmin
          .acceptLogoutRequest(challenge)
          .then(({ data: body }) => {
            // All we need to do now is to redirect the user back to hydra!
            // Important: If id_token_hint (oidc id token issued by hydra during login) is not provided, redirect_to will not pick up uri declared by client app.
            res.redirect(String(body.redirect_to))
          })
          .catch(next)
      }

      res.render('logout', {
        csrfToken: req.csrfToken(),
        challenge: challenge,
        action: urljoin(process.env.BASE_URL || '', '/logout')
      })
    })
    .catch(next)
})

router.post('/', csrfProtection, (req, res, next) => {
  // The challenge is now a hidden input field, so let's take it from the request body instead
  const challenge = req.body.challenge

  if (req.body.submit === 'No') {
    return (
      hydraAdmin
        .rejectLogoutRequest(challenge)
        .then(() => {
          // TODO: The user did not want to log out. Let's redirect him back somewhere or do something else.
          res.redirect('https://www.ory.sh/')
        })
        .catch(next)
    )
  }

  // The user agreed to log out, let's accept the logout request.
  hydraAdmin
    .acceptLogoutRequest(challenge)
    .then(({ data: body }) => {
      // All we need to do now is to redirect the user back to hydra!
      res.redirect(String(body.redirect_to))
    })
    .catch(next)
})

export default router
