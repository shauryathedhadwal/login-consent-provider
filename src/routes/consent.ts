import express from 'express'
import url from 'url'
import urljoin from 'url-join'
import csrf from 'csurf'
import { hydraAdmin } from '../config'
import { ConsentRequestSession } from '@oryd/hydra-client'
import { getUserProfile } from '../rpc/userManager'

const csrfProtection = csrf({ cookie: true })
const router = express.Router()

const getIdTokenClaims = async (phonenumber: string, scopes: string[]) => {
  const isProfileRequested = scopes?.indexOf('profile')
  if (isProfileRequested < 0 || undefined) return {}
  try {
    const res = await getUserProfile(phonenumber)
    return res.data
  } catch (error) {
    return {}    
  }
}

router.get('/', csrfProtection, (req, res, next) => {
  const query = url.parse(req.url, true).query
  // The challenge is used to fetch information about the consent request from ORY hydraAdmin.
  const challenge = String(query.consent_challenge)
  if (!challenge) {
    next(new Error('Expected a consent challenge to be set but received none.'))
    return
  }

  hydraAdmin
    .getConsentRequest(challenge)
    .then(async ({ data: body }) => {
      // Check whether current request is from trusted client
      // If yes, then skip UI and accept the consent form
      // TODO: Add logic to validate source of request (trusted or third party)
      const skip = process.env.SKIP_CONSENT === '1'

      const session: ConsentRequestSession = { access_token: {}, id_token: {} }
      session.id_token = await getIdTokenClaims(body.subject as string, body.requested_scope || [])

      if (skip) {
        return hydraAdmin
          .acceptConsentRequest(challenge, {
            // TODO: Add default scopes
            grant_scope: body.requested_scope,
            grant_access_token_audience: body.requested_access_token_audience,
            session: {
              // This data will be available when introspecting the token. Try to avoid sensitive information here,
              // accessToken: { foo: 'bar' },
              // idToken: { baz: 'bar' },
              ...session
            }
          })
          .then(({ data: body }) => {
            res.redirect(String(body.redirect_to)) //Redirect back to Hydra
          })
      }

      // If consent can't be skipped we MUST show the consent UI.
      res.render('consent', {
        csrfToken: req.csrfToken(),
        challenge: challenge,
        // We have a bunch of data available from the response, check out the API docs to find what these values mean
        // and what additional data you have available.
        requested_scope: body.requested_scope,
        user: body.subject,
        client: body.client,
        action: urljoin(process.env.BASE_URL || '', '/consent')
      })
    })
    // This will handle any error that happens when making HTTP calls to hydra
    .catch(next)
  // The consent request has now either been accepted automatically or rendered.
})

router.post('/', csrfProtection, (req, res, next) => {
  // The challenge is now a hidden input field, so let's take it from the request body instead
  const challenge = req.body.challenge

  // Let's see if the user decided to accept or reject the consent request..
  if (req.body.submit === 'Deny access') {
    // Looks like the consent request was denied by the user
    return (
      hydraAdmin
        .rejectConsentRequest(challenge, {
          error: 'access_denied',
          error_description: 'The resource owner denied the request'
        })
        .then(({ data: body }) => {
          // All we need to do now is to redirect the browser back to hydra!
          res.redirect(String(body.redirect_to))
        })
        // This will handle any error that happens when making HTTP calls to hydra
        .catch(next)
    )
  }
  // label:consent-deny-end

  let grantScope = req.body.grant_scope
  if (!Array.isArray(grantScope)) {
    grantScope = [grantScope]
  }

  // The session allows us to set session data for id and access tokens
  let session: ConsentRequestSession = {
    // This data will be available when introspecting the token. Try to avoid sensitive information here,
    // unless you limit who can introspect tokens.
    access_token: {
      // foo: 'bar'
    },

    // This data will be available in the ID token.
    id_token: {
      // baz: 'bar'
    }
  }

  // Here is also the place to add data to the ID or access token. For example,
  // if the scope 'profile' is added, add the family and given name to the ID Token claims:
  // if (grantScope.indexOf('profile')) {
  //   session.id_token.family_name = 'Doe'
  //   session.id_token.given_name = 'John'
  // }

  // Let's fetch the consent request again to be able to set `grantAccessTokenAudience` properly.
  hydraAdmin
    .getConsentRequest(challenge)
    // This will be called if the HTTP request was successful
    .then(({ data: body }) => {
      return hydraAdmin
        .acceptConsentRequest(challenge, {
          // We can grant all scopes that have been requested - hydra already checked for us that no additional scopes
          // are requested accidentally.
          grant_scope: grantScope,

          // If the environment variable CONFORMITY_FAKE_CLAIMS is set we are assuming that
          // the app is built for the automated OpenID Connect Conformity Test Suite. You
          // can peak inside the code for some ideas, but be aware that all data is fake
          // and this only exists to fake a login system which works in accordance to OpenID Connect.
          //
          // If that variable is not set, the session will be used as-is.
          // session: oidcConformityMaybeFakeSession(grantScope, body, session),
          session: session,

          // ORY Hydra checks if requested audiences are allowed by the client, so we can simply echo this.
          grant_access_token_audience: body.requested_access_token_audience,

          // This tells hydra to remember this consent request and allow the same client to request the same
          // scopes from the same user, without showing the UI, in the future.
          remember: Boolean(req.body.remember),

          // When this "remember" sesion expires, in seconds. Set this to 0 so it will never expire.
          remember_for: +(process.env.CONSENT_SESSION_EXPIRES_IN_SECONDS || "0")
        })
        .then(({ data: body }) => {
          // All we need to do now is to redirect the user back to hydra!
          res.redirect(String(body.redirect_to))
        })
    })
    // This will handle any error that happens when making HTTP calls to hydra
    .catch(next)
  // label:docs-accept-consent
})

export default router
