import axios from 'axios'

const RPC_URL: string = process.env.USER_MANAGER_URL + '/api'

const validateCredentials = (mobile: number | string, password: string) => {
    const url = RPC_URL + '/user/' + mobile + '/validate'
    return axios.post(url, { password })
}

const getUserProfile = (phonenumber: number | string) => {
    const url = RPC_URL + '/user/' + phonenumber
    return axios.get(url)
}

export { validateCredentials, getUserProfile }