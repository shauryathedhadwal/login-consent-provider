import { NextFunction, Request, Response } from "express";

export const RequestHandler = (fn: (req: Request, res: Response, next: NextFunction) => any) => fn