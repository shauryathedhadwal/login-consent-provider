// Declare DOM Element references

let phoneNumberEl
let passwordEl
let submitButtonEl
let phoneNumberErrorEl
let passwordErrorEl
let formEl

const validateForm = (phoneNumber, password) => {
    const retVal = {
        success: true,
        phoneNumber: '',
        password: ''
    }
    if (!phoneNumber) {
        retVal.phoneNumber = 'Enter Mobile Number'
        retVal.success = false
    }
    if (!password) {
        retVal.password = 'Enter Password'
        retVal.success = false
    }

    return retVal
}

const stripText = (text) => {
    if (!text) {
        return null
    }
    const strippedText = text.trim()
    if (strippedText === '') {
        return null
    }
    return strippedText
}

const updateErrorFieldsVisibility = (isHidden = true) => {
    phoneNumberErrorEl.style.visibility = isHidden ? "hidden" : "visible"
    passwordErrorEl.style.visibility = isHidden ? "hidden" : "visible"
}

const onFormSubmit = (event) => {
    const phoneNumber = stripText(phoneNumberEl.value)
    const password = stripText(passwordEl.value)
    const validationResult = validateForm(phoneNumber, password)

    if (!validationResult.success) {
        phoneNumberErrorEl.textContent = validationResult.phoneNumber
        passwordErrorEl.textContent = validationResult.password
        updateErrorFieldsVisibility(false)
        event.preventDefault()
    }
}

// Run logic after DOM has been parsed
window.addEventListener('DOMContentLoaded', (event) => {
    phoneNumberEl = document.getElementById('input-phonenumber')
    passwordEl = document.getElementById('input-password')
    submitButtonEl = document.getElementById('form-submit')
    phoneNumberErrorEl = document.getElementById('error-phonenumber')
    passwordErrorEl = document.getElementById('error-password')
    formEl = document.getElementById('form')

    formEl.submit = onFormSubmit
    formEl.addEventListener('submit', onFormSubmit)
})